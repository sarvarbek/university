import express from "express"
import apiRoute from "./route/api.route";

const PORT = 8080;
export const app = express()
app.use(express.json())
app.use('/api', apiRoute)
app.listen(PORT, () => console.log(`Port running on port:  ${PORT}`))
