import {FacultyController} from "../controller/faculty-controller";
import {StudentController} from "../controller/student-controller"
import {Router} from "express";
import {MainImplRoute} from "./main-impl.route";
import {GroupController} from "../controller/group-controller";
import {SectionController} from "../controller/section-controller";


const apiRoute = Router();

const studentRoute = new MainImplRoute(new StudentController())
const groupRoute = new MainImplRoute(new GroupController())
const facultyRoute = new MainImplRoute(new FacultyController())
const sectionRoute = new MainImplRoute(new SectionController())

apiRoute.use("/student", studentRoute.route)
apiRoute.use("/group", groupRoute.route)
apiRoute.use("/faculty", facultyRoute.route)
apiRoute.use("/section", sectionRoute.route)

export default apiRoute
