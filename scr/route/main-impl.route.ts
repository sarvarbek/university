import {BaseController} from "../controller/base-controller";
import {Router} from "express";

export class MainImplRoute{
    route = Router()
    constructor(private controller: BaseController) {
        this.route.post('/', this.controller.create),
        this.route.get('/', this.controller.getAll),
        this.route.get('/', this.controller.getById)
        this.route.put('/', this.controller.update),
        this.route.delete('/', this.controller.delete)
    }
}
