"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.app = void 0;
const express_1 = __importDefault(require("express"));
const api_route_1 = __importDefault(require("./route/api.route"));
const PORT = 8080;
exports.app = (0, express_1.default)();
exports.app.use(express_1.default.json());
exports.app.use('/api', api_route_1.default);
exports.app.listen(PORT, () => console.log(`Port running on port:  ${PORT}`));
//# sourceMappingURL=index.js.map